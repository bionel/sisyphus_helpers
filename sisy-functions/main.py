from PyQt5.QtWidgets import QMessageBox
from test_functions import *
import PyQt5
from PyQt5 import QtWidgets

# Main code here

for line in getMirrors():
    if line['isActive']: mirror = line['Url']

pkgIndex = str(mirror + 'Packages')
rmtCsvUrl = str(mirror.replace('packages', 'csv') + 'remotePackagesPre.csvx')
rmtDscUrl = str(mirror.replace('packages', 'csv') + 'remoteDescriptionsPre.csv')

print('pkgIndex: ', pkgIndex)
print('rmtCsvUrl: ', rmtCsvUrl)
print('rmtDscUrl: ', rmtDscUrl)

def showErrorMessage(err):
    app = QtWidgets.QApplication([])
    # msg = QMessageBox()
    # msg.setIcon(QMessageBox.Critical)
    # msg.setText("Error")
    # msg.setInformativeText(err)
    # msg.setWindowTitle("Error")
    # msg.exec()
    error_dialog = QtWidgets.QErrorMessage()
    error_dialog.showMessage(str(err))
    app.exec()

try:
    columns = 'category,name,version,slot'
    check_ts = checkRemoteFile(rmtCsvUrl, 'remotePackages')
    if check_ts is not None:
        if updateRemoteTable(rmtPkgCsv, rmtCsvUrl, 'remote_packages', columns):
            updateFileStamp('remotePackages', check_ts)
except ConnectionError as err:
    print('error1 catched!! ', str(err))
    showErrorMessage(str(err))

#
# try:
#     columns = 'category,name,description'
#     check_ts = checkRemoteFile(rmtDscUrl, 'remoteDescriptions')
#     if check_ts is not None:
#          if updateRemoteTable(rmtDscCsv, rmtDscUrl,'remote_descriptions', columns):
#              updateFileStamp('remoteDescriptions', check_ts)
# except ConnectionError as err:
#     print('error2 catched!! ', err)
#
# try:
#     check_ts = checkRemoteFile(pkgIndex, 'packagesIndex')
#     if check_ts is not None:
#         print('trigger emerge sync')
#         updateFileStamp('packagesIndex', check_ts)
# except ConnectionError as err:
#     print('error3 catched!! ', err)

print(getSystemMode())