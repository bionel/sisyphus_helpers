#!/usr/bin/python3

import csv
import shutil
import sqlite3
import urllib3
import base64
import os
from socket import gaierror

rmtPkgCsv = 'remotePackagesPre.csv'
rmtDscCsv = 'remoteDescriptionsPre.csv'
lclPkgCsv = 'localPackagesPre.csv'
sisyphusDB = 'sisyphus.db'
mirrorCfg = '/etc/sisyphus/mirrors.conf'

def getMirrors():
    mirrorList = []
    with open(mirrorCfg) as mirrorFile:
        for line in mirrorFile.readlines():
            if 'PORTAGE_BINHOST=' in line:
                url = line.split("=")[1].replace('"', '').rstrip()
                mirror = {'isActive':True,'Url':url}
                if line.startswith('#'):
                    mirror['isActive'] = False
                mirrorList.append(mirror)
    mirrorFile.close()
    return mirrorList

def getSystemMode():
    portage_binmode_make_conf = '/opt/redcore-build/conf/intel/portage/make.conf.amd64-binmode'
    portage_mixedmode_make_conf = '/opt/redcore-build/conf/intel/portage/make.conf.amd64-mixedmode'
    portage_srcmode_make_conf = '/opt/redcore-build/conf/intel/portage/make.conf.amd64-srcmode'
    portage_make_conf_symlink = '/etc/portage/make.conf'
    mode = 'unknown'
    if os.path.islink(portage_make_conf_symlink):
        if os.path.realpath(portage_make_conf_symlink) == portage_binmode_make_conf:
            mode = 'binary'
        elif os.path.realpath(portage_make_conf_symlink) == portage_mixedmode_make_conf:
            mode = 'mixed'
        elif os.path.realpath(portage_make_conf_symlink) == portage_srcmode_make_conf:
            mode = 'source'
    return mode

def updateFileStamp(stamp_key, stamp_value):
    print('updating ts')
    db = sqlite3.connect(sisyphusDB)
    cursor = db.cursor()
    cursor.execute('''insert or replace into stamps (remotefile, stamp) values (
    ifnull((select remotefile from stamps where remotefile = ?),?),?);
    ''', (stamp_key, stamp_key, stamp_value))
    db.commit()
    db.close()

def checkRemoteFile(remoteFile, stamp_key):
    http = urllib3.PoolManager()
    try:
        req = http.request('HEAD', remoteFile)
    except (urllib3.exceptions.MaxRetryError, urllib3.exceptions.NewConnectionError, gaierror):
        raise ConnectionError('No network connection!')
    if req.status == 200:
        print(req.headers['last-modified'])
        rts = base64.b64encode(req.headers['last-modified'].encode()).decode("utf-8")
        db = sqlite3.connect(sisyphusDB)
        cursor = db.cursor()
        cursor.execute('''create table if not exists stamps (remotefile TEXT UNIQUE, stamp TEXT)''')
        cursor.execute('''select stamp from stamps where remotefile = ?''', (stamp_key,))
        lts = cursor.fetchone()
        db.commit()
        db.close()
        if lts is not None:
            lts = ''.join(lts)
            print('lts:', lts)
            if rts == lts:
                print('no update needed')
                return None
            else:
                print('update needed')
                return rts
        else:
            print('stamp not found, forcing update')
            return rts
    else:
        raise ConnectionError('Repository not available!')
        return None

def fetchRemoteCSV(remoteFile, localFile):
    print('fetching remote csv')
    http = urllib3.PoolManager()
    try:
        req = http.request('GET', remoteFile , preload_content=False)
    except (urllib3.exceptions.MaxRetryError, urllib3.exceptions.NewConnectionError, gaierror):
        raise ConnectionError('No network connection!')
    if req.status == 200:
        with req as tmp_buffer, open(localFile, 'wb') as output_file:
            shutil.copyfileobj(tmp_buffer, output_file)
        return True
    else:
        raise ConnectionError('Repository not available!')
        return False

def updateRemoteTable(localFile, remoteFile, dbTable, columns):
    if fetchRemoteCSV(remoteFile, localFile):
        print('updating ', dbTable, ' table from ', localFile)
        pattern = []
        for p in range(len(columns.split(','))): pattern.append('?')
        pattern = ','.join(pattern)
        db = sqlite3.connect(sisyphusDB)
        cursor = db.cursor()
        cursor.execute('''drop table if exists %s''' % dbTable)
        cursor.execute('''create table %s (%s)''' % (dbTable, columns))
        with open(localFile) as src:
             for row in csv.reader(src):
                 cursor.execute("insert into %s (%s) values (%s);" % (dbTable, columns, pattern), row)
        db.commit()
        db.close()
        return True
    else:
        return False
