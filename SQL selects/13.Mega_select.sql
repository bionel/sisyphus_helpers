/* 
Join all tables, all packages
*/
SELECT
      CASE WHEN i1.category IS NOT NULL THEN i1.category ELSE a1.category END AS Category,
      CASE WHEN i1.name IS NOT NULL THEN i1.name ELSE a1.name END AS PkgName,
      IFNULL(i1.version, 'not installed') AS i_Ver,
      IFNULL(a1.version, 'not available') AS a_Ver,
      CASE WHEN w1.name IS NULL THEN 'no' ELSE 'yes' END AS isRemovable,
      IFNULL(i1.description, a1.description) AS descr
FROM local_packages AS i1
LEFT JOIN remote_packages AS a1
    ON a1.category = i1.category AND a1.name = i1.name AND a1.slot = i1.slot
LEFT JOIN world_packages_demo AS w1
    ON w1.category = i1.category AND w1.name = i1.name
--WHERE i1.name LIKE '%jq%'
--WHERE a1.version IS NULL
--WHERE w1.name IS NOT NULL
UNION
SELECT
      CASE WHEN i2.category IS NOT NULL THEN i2.category ELSE a2.category END AS Category,
      CASE WHEN i2.name IS NOT NULL THEN i2.name ELSE a2.name END AS PkgName,
      IFNULL(i2.version, 'not installed') AS i_Ver,
      IFNULL(a2.version, 'not available') AS a_Ver,
      CASE WHEN w2.name IS NULL THEN 'no' ELSE 'yes' END AS isRemovable,
      IFNULL(i2.description, a2.description) AS descr
FROM remote_packages AS a2
LEFT JOIN local_packages AS i2
    ON a2.category = i2.category AND a2.name = i2.name AND a2.slot = i2.slot
LEFT JOIN world_packages_demo AS w2
    ON w2.category = i2.category AND w2.name = i2.name
--WHERE i2.name LIKE '%jq%'
--WHERE a2.version IS NULL
--WHERE w2.name IS NOT NULL
;
