/* 
Search installed packages with availabe version
and removable status
Packages not available will get a_ver = NULL
*/

SELECT
--       CASE WHEN i.category IS NOT NULL THEN i.category ELSE a.category END AS Category,
--       CASE WHEN i.name IS NOT NULL THEN i.name ELSE a.name END AS Pkg_Name,
      i.category||"/"||i.name AS 'cat/PN',
      IFNULL(i.version, 'not installed') AS i_Ver,
      IFNULL(a.version, 'not available') AS a_Ver,
      CASE WHEN w.name ISNULL THEN 'no' ELSE 'yes' END AS isRemovable,
      IFNULL(i.description, a.description) AS descr
  FROM local_packages AS i
  LEFT JOIN remote_packages AS a
      ON i.category = a.category
      AND i.name = a.name
      AND i.slot = a.slot
      
  LEFT JOIN world_packages_demo AS w
      ON i.category = w.category
      AND i.name = w.name
/*
Enable this to show packages installed but not available
e.g.installed in src_mode
*/
--    WHERE  -- a.version IS NULL
/*search by name*/ 
-- i.name LIKE "%jq%"
/*search removable/system */   
-- w.name IS NOT NULL
  
;
