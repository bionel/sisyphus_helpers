/* 
Search installed packages with available version.
This will return only packages installed AND available,
a.k.a only installed in binmode
Use it only on specific case
*/

SELECT
      i.category AS cat,
      i.name AS pn,
      i.version AS iv, 
      a.version AS av,
      i.description AS descr

  FROM local_packages AS i
  INNER JOIN remote_packages AS a
  ON a.category = i.category
  AND a.name = i.name
  AND a.slot = i.slot
--  WHERE

-- search by category  
--    i.category LIKE "%net%" OR i.category LIKE "%www%"

-- search by name  
--    i.name LIKE "%chrome%"

-- search by description  
-- descr LIKE "%%"
  
;
