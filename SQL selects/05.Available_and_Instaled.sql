/* 
Search available packages with installed version.
This will return only packages installed AND available,
a.k.a only installed in binmode,
therefore use it only on specific case
*/

SELECT
      a.category AS cat,
      a.name AS pn,
      a.version AS av, 
      i.version AS iv,
      a.description AS descr
  FROM remote_packages AS a
  INNER JOIN local_packages AS i
  ON a.category = i.category
  AND a.name = i.name
  AND a.slot = i.slot
--  WHERE

-- search by category  
--    cat LIKE "%www%" OR cat LIKE "%net%"

-- search by name  
--    a.name LIKE "%docbook-xml-dtd%"

-- search by description  
--    descr LIKE "%video%"
  
;
