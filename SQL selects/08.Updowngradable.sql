/* 
Search "upgradable" packages
This may show downgradable as well
*/

SELECT
      i.category AS cat,
      i.name AS pn,
      i.version AS iv, 
      a.version AS av,
      i.description AS descr
  FROM local_packages AS i
  INNER JOIN remote_packages AS a
  ON a.category = i.category
  AND a.name = i.name
  AND a.slot = i.slot
  WHERE

--updowngradable
--  av <> iv

--downgradable
--  av < iv

--upgradable
  av > iv
;
