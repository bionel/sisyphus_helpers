/* 
Search ALL installed packages with corresponding available version
Packages not available will get av = NULL
*/

SELECT distinct
      i.category AS cat,
      i.name AS pn,
      i.version AS iv, 
      a.version AS av,
      i.description AS descr
  FROM local_packages AS i
  LEFT JOIN remote_packages AS a
  ON a.category = i.category
  AND a.name = i.name
  AND a.slot = i.slot
--  WHERE 

/*
Enable this line to show only packages installed in src_mode
*/
-- a.version IS NULL

-- search by category  
--    cat LIKE "%www%" OR cat LIKE "%net%"

-- search by name  
--    a.name LIKE "%docbook-xml-dtd%"

-- search by description  
--    descr LIKE "%video%"
;
