/* 
Search removable packages
a.k.a installed @world
*/

SELECT
      i.category||'/'||i.name AS 'Cat/PN',
      i.version AS i_ver,
--      i.slot AS i_slot, i.category AS i_cat, i.name AS i_name, i.version AS i_ver,
      CASE WHEN w.name ISNULL THEN 'no' ELSE 'yes' END AS isRemovable,
      i.description AS descr,
            
      w.name AS w_name, w.category AS w_cat
      /*w.name AS 'w/name',*/  
      
  FROM local_packages AS i

  LEFT JOIN world_packages_demo AS w
  ON w.category = i.category
  AND w.name = i.name
  
--  AND w.description = i.description
  
--WHERE w_cat IS NULL
--AND 
--i_name LIKE "%jq%"

;
