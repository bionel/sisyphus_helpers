/* 
Search available packages with corresponding installed version
Packages not installed will get iv = NULL
*/

SELECT
      a.category AS cat,
      a.name AS pn,
      a.version AS av, 
      i.version AS iv,
      a.description AS descr
  FROM remote_packages AS a
  LEFT JOIN local_packages AS i
  ON a.category = i.category
  AND a.name = i.name
  AND a.slot = i.slot

/*
Enable this to show only available as binpkg but not instaled
Sort of "instalable" packages
*/
--  WHERE i.version IS NULL
  
;
