/* 
Search available packages with corresponding installed version
and if removable
Packages not installed will get iv = NULL
*/

SELECT
      a.category||"/"||a.name AS 'cat/PN',
      a.version AS av_Ver, 
      IFNULL(i.version, 'not installed') AS inst_Ver,
      CASE WHEN w.name ISNULL THEN 'no' ELSE 'yes' END AS isRemovable,
      a.description AS descr
  FROM remote_packages AS a
  LEFT JOIN local_packages AS i
      ON a.category = i.category
      AND a.name = i.name
      AND a.slot = i.slot      
  LEFT JOIN world_packages_demo AS w
      ON i.category = w.category
      AND i.name = w.name

/* Filters */
-- WHERE 
--a.name LIKE "%jq%" OR i.name LIKE "%jq%"
  
-- w.name IS NULL AND i.name IS NOT NULL
  
;
